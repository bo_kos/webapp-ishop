package servlet.ajax;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlet.AbstractController;
import servlet.util.RoutingUtils;

@WebServlet("/dvs/ajax/html/more/search")
public class SearchMoreController extends AbstractController {
    private static final long serialVersionUID = -1761511113818731739L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	req.setAttribute("productCount", 24);
	RoutingUtils.forwardToFragment("product-list.jsp", req, resp);
    }

}
