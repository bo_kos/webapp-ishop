package servlet.page;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlet.AbstractController;
import servlet.util.RoutingUtils;

@WebServlet("/error")
public class ErrorController extends AbstractController {
    private static final long serialVersionUID = 4061746766421044827L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	RoutingUtils.forvardToPage("error.jsp", req, resp);
    }
}
