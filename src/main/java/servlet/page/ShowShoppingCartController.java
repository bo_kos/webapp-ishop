package servlet.page;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlet.AbstractController;
import servlet.util.RoutingUtils;

@WebServlet("/shopping-cart")
public class ShowShoppingCartController extends AbstractController {
    private static final long serialVersionUID = 4348617630397784205L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	RoutingUtils.forvardToPage("shopping-cart.jsp", req, resp);
    }

}
