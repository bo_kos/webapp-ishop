package servlet.page;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlet.AbstractController;
import servlet.util.RoutingUtils;

@WebServlet("/products/*")
public class ProductsByCategoryController extends AbstractController {
    private static final long serialVersionUID = -2118792225128248997L;
    private static final int SUBSTRING_INDEX = "/dvs/products".length();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	String categoryUrl = req.getRequestURI().substring(SUBSTRING_INDEX);
	RoutingUtils.forvardToPage("products.jps", req, resp);
    }

}
