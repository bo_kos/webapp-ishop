package filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlet.util.RoutingUtils;

@WebFilter(filterName = "ErrorHandlerFilter")
public class ErrorHandlerFilter extends AbstractFilter {

    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
	    throws IOException, ServletException {
	try {
	    chain.doFilter(req, resp);
	} catch (Throwable e) {
	    String url = req.getRequestURI();
	    RoutingUtils.forvardToPage("error.jsp", req, resp);
	}
    }

    @Override
    public void destroy() {
    }
}
