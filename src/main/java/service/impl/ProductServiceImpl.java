package service.impl;

import entity.Product;
import exeption.InternalServerError;
import jdbc.JDBCUtils;
import jdbc.ResultSetHandler;
import jdbc.ResultSetHandlerFactory;
import service.ProductService;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

class ProductServiceImpl implements ProductService {

    private  final DataSource dataSource;

    public ProductServiceImpl(DataSource dataSource) {
        super();
        this.dataSource = dataSource;
    }

    private static final ResultSetHandler<List<Product>> productResultSetHandler =
        ResultSetHandlerFactory.getListResultSetHandler(ResultSetHandlerFactory.PRODUCT_RESULT_SET_HANDLER);

    @Override
    public List<Product> listAllProducts(int page, int limit) {
        try(Connection c = dataSource.getConnection()) {
            int offset = (page - 1) * limit;
            return JDBCUtils.select(c,"SELECT p.*, c,name as ishop.new_schema.category, pr.name as ishop.new_schema.producer from ishop.new_schema.product p, ishop.new_schema.producer pr, ishop.new_schema.category c WHERE c.id=p.id_category and pr.id=p.id_producer limit ? offset ?", productResultSetHandler,limit,offset);
        } catch (SQLException e) {
            throw new InternalServerError("Can't execute sql query: " + e.getMessage(), e);
        }
    }
}
