package service;

import entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> listAllProducts(int page, int limit);
}
