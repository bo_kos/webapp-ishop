package model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class Model implements Serializable {
    private static final long serialVersionUID = -6359549900797572596L;
    private List<String> list = Arrays.asList(new String[] { "first", "second", "thrird" });

    public List<String> getList() {
	return list;
    }
}
