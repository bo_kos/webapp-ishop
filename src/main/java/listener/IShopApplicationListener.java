package listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import service.impl.ServiceManager;

public class IShopApplicationListener implements ServletContextListener {
    private ServiceManager serviceManager;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
	try {
	    serviceManager = ServiceManager.getInstance(sce.getServletContext());
	} catch (Exception e) {
	    throw e;
	}
	serviceManager = ServiceManager.getInstance(sce.getServletContext());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
	serviceManager.close();
    }
}
